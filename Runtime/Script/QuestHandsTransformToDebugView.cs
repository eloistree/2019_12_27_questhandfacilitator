﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestHandsTransformToDebugView : MonoBehaviour
{
    public QuestHandsStateTransformMono m_handStateInfo;
    public Text m_textFieldDebug;
    [TextArea(3,52)]
    public string m_compressTextDebug="";
    public float m_delay=0.1f;

    void Start()
    {
        InvokeRepeating("PrintInfo", 0, m_delay);
    }

    void PrintInfo()
    {
        m_compressTextDebug = QuestHandsUtility.CompressQuestHandsTransformToKeyStateAsString(m_handStateInfo.m_anchorReferences);
        if (m_textFieldDebug != null)
        {
            m_textFieldDebug.text = m_compressTextDebug;
        }


    }
}
