﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHandBonePosition : MonoBehaviour
{
    public Transform m_targetToMove;
    public QuestHandsStateTransformMono m_handsToFollow;
    public HandType m_handToFollow;
    public FingerBones m_fingerToFollow = FingerBones.HandIndexTip;
    
    void LateUpdate()
    {
        if (m_handsToFollow == null || m_handsToFollow == null)
            return;
        m_targetToMove.position = m_handsToFollow.GetTransformState().GetWorldPositionOf(m_handToFollow, m_fingerToFollow);
        m_targetToMove.rotation = m_handsToFollow.GetTransformState().GetWorldRotationOf(m_handToFollow, m_fingerToFollow);

    }
    private void Reset()
    {
        m_targetToMove = this.transform;
    }
}
