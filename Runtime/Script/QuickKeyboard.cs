﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickKeyboard : MonoBehaviour
{

    public string m_stringRecorded;
    public Text m_debugText;
    public void Start()
    {

        RefreshDebug();
    }
    public string GetCurrentRecord() { return m_stringRecorded; }
    public void RemoveCharacter() {
        if(m_stringRecorded.Length>0)
           m_stringRecorded = m_stringRecorded.Substring(0, m_stringRecorded.Length - 1);
        RefreshDebug();
    }
    public void ClearRecord() {
        m_stringRecorded = "";
        RefreshDebug();
    }

    public void Append(string text) {
        m_stringRecorded += text;
        RefreshDebug();
    }

    public void RefreshDebug() {
        if (m_debugText)
            m_debugText.text = m_stringRecorded;
    }
}
