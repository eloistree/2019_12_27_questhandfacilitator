﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  QuestHandsStateTransformMono : MonoBehaviour
{
    public QuestHandsStateTransform m_anchorReferences;

    public QuestHandsStateTransform GetTransformState()
    {
        return m_anchorReferences;
    }
    public string GetStringCompressedVersion()
    {
        return m_anchorReferences.GetStringCompressedVersion();
    }
    public void  ReconstructFromStringVersion(string compressedValue, QuestHandsStateTransform.HandsTransformDisplayType relocationType)
    {
         m_anchorReferences.SetFromStringCompressedVersion(compressedValue, relocationType);
    }
}
