﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestHandStateDebugView : MonoBehaviour
{
    public QuestHandsStateTransformMono m_questHandsState;
    [TextArea(2,5)]
    public string m_compressedState;
    public QuestHandsStateKey m_instanceState = new QuestHandsStateKey();
    
    void Update()
    {
        m_questHandsState.GetTransformState().GetDataFromTransform(ref m_instanceState);
        m_compressedState = QuestHandsUtility.CompressQuestHandsKeyStateAsString(m_instanceState);
        
    }

    private void Reset()
    {
        m_questHandsState = GetComponent<QuestHandsStateTransformMono>();
    }
}
