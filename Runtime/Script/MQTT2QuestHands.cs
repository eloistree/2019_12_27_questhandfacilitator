﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MQTT2QuestHands : MonoBehaviour
{
    public UserMQTT m_client;
    public QuestHandsStateTransformMono m_questHandsMono;
    public string m_lastHandInfo;
    public bool m_onlyOutsideQuest=true;

    public void Start()
    {
        if(!m_onlyOutsideQuest || (m_onlyOutsideQuest && Application.platform!= RuntimePlatform.Android))
            {
            m_client.m_debugPackage.AddListener(MessageToHand);
        }
    }

    private void MessageToHand(MQTT_UserPackageReceived arg0)
    {
        m_lastHandInfo = arg0.m_message;
    }

    public void LateUpdate()
    {
        string line = m_lastHandInfo;
        QuestHandsStateKey data = QuestHandsUtility.ReconstructStateFromCompressedString(line);
        m_questHandsMono.m_anchorReferences.SetFromDataToTransform(data);
    }

}
