﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class HandsMovementRecorder : MonoBehaviour
{
    public QuestHandsStateTransformMono m_handsTracked;
    [Range(0f,1f)]
    public float m_timeBetweenKeyRecord=0.00f;
    public float m_maxRecordingTime = 60f;
    [Header("Debug")]
    public bool m_isRecording;

    public float m_timerSinceRecording;
    public float m_frameCountDown;


    public OculusHandsRecordAsString m_currentRecord = new OculusHandsRecordAsString();
    public OculusHandsRecordEvent m_onFinishRecord;

    [HideInInspector] public string m_nameOfTheRecord = "lastrecord";
    [HideInInspector] public string m_whereToSaveLocal = "QuestHandsRecord/Movement";

    public void ToogleRecording() {
        if (m_isRecording)
            StopRecording();
        else StartRecordingWithTimeAsName();
    }
    public void StartRecordingWithTimeAsName()
    {
        StartRecording("HandRecord_"+DateTime.Now.ToString("yyyy_dd_MM_HH_mm_ss"));
        m_timerSinceRecording = 0;

    }
    public void StartRecording(string titleName)
    {
        m_currentRecord.Clear();
        m_nameOfTheRecord = titleName;
        m_isRecording = true;
    }
    public void StopRecording() {
        m_onFinishRecord.Invoke(m_currentRecord);
        m_isRecording = false;
    }
    public void Update()
    {
        if (!m_isRecording)
            return;
        m_timerSinceRecording += Time.deltaTime;
        m_frameCountDown += Time.deltaTime;
        if (m_frameCountDown > m_timeBetweenKeyRecord || m_timeBetweenKeyRecord<=0f) {
            m_frameCountDown = 0;
            RecordKey();

        }
        if (m_maxRecordingTime > 0) {
            if (m_timerSinceRecording >= m_maxRecordingTime)
                StopRecording();
        }
    }

    private void RecordKey()
    {
        m_currentRecord.AddRecordKey( m_timerSinceRecording, m_handsTracked.GetTransformState().GetStringCompressedVersion());
    }
}



[System.Serializable]
public class OculusHandsRecordEvent: UnityEvent<OculusHandsRecordAsString> {

}
public class OculusHandsRecordAsString{
    
    public string m_recordKeys="";
    private void AddRecordKey(float time, QuestHandsStateTransformMono hand)
    {
        AddRecordKey(time, hand.GetStringCompressedVersion());
    }
    public void AddRecordKey(float time, string compressedStringVersion)
    {
        m_recordKeys += string.Format("{0:0.0000}#{1}\n", time, compressedStringVersion);
    }
    public static List<HandsRecordKey> GetKeys(string compressText) {
        List<HandsRecordKey> keys = new List<HandsRecordKey>();
        string[] lines = compressText.Split('\n');
        for (int i = 0; i < lines.Length; i++)
        {
            string[] tokenTimeCompress = lines[i].Split('#');
            if (tokenTimeCompress.Length == 2) {
                float time = float.Parse(tokenTimeCompress[0]);
                string compress = tokenTimeCompress[1];
                keys.Add(new HandsRecordKey() { m_time = time, m_compressedVersion = compress });
            }

        }
        return keys;
    }

    internal void Clear()
    {
        m_recordKeys  = "";
    }
}

public class HandsMovementAsKeys {

    public HandsMovementAsKeys(List<HandsRecordKey> keys) {
        m_keys = keys;
        if (m_keys.Count <= 0)
            m_keys.Add(new HandsRecordKey());
    }

    public List<HandsRecordKey> m_keys= new List<HandsRecordKey>();
    public HandsRecordKey GetFirst() { return m_keys[0]; }
    public HandsRecordKey GetLast() { return m_keys[m_keys.Count-1]; }

    public void RemoveTimeAtStart() {
        RemoveTimeToAll(GetFirst().GetTime());
    }
    public void RemoveTimeToAll(float time) {
        AddTimeToAll(-time);
    }
    public void AddTimeToAll(float time) {
        for (int i = 0; i < m_keys.Count; i++)
        {
            m_keys[i].m_time += time;
        }
    }
    public void Order()
    {
        m_keys = m_keys.OrderBy(k => k.m_time).ToList();


    }

    public bool GetNextKey(float time, out HandsRecordKey key)
    {
        key = null;
        for (int i = 0; i < m_keys.Count; i++)
        {
            if (m_keys[i].m_time > time)
            {
                key = m_keys[i];
                return true;
            }
        }
        return false;
    }



    public float GetTotalTime() {
        return GetLast().GetTime() - GetFirst().GetTime();
    }

    public int GetCount()
    {
        return m_keys.Count;
    }
}
public class HandsRecordKey
{
    public float m_time=0f;
    public string m_compressedVersion;
    public float GetTime() { return m_time; }
    public string GetCompressedRecord() { return m_compressedVersion; }
    public QuestHandsStateKey GetHandsState() { return QuestHandsUtility.ReconstructStateFromCompressedString(m_compressedVersion); }
}