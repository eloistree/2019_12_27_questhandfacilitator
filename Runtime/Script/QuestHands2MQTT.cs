﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

public class QuestHands2MQTT : MonoBehaviour
{
   
    public UserMQTT m_client;
    public QuestHandsStateTransformMono m_questHandsMono;
    public float m_timeBetweenMessage=0.1f;
    public string m_lastReceived;
    void Start()
    {
        StartCoroutine( SendInfo());
    }

    public void SetTimeBetweenMessage(float time)
    {
        m_timeBetweenMessage = time;
    }
    public void SetFrequenceBetweenMessage(int frequence)
    {
        m_timeBetweenMessage = 1f / (float)frequence;
    }


    public IEnumerator SendInfo()
    {
        while(true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_timeBetweenMessage);
            bool isOnAndroid = Application.platform == RuntimePlatform.Android; // + An on Quest
            if (isOnAndroid) {
                Debug.Log("sending...");
                m_client.Send("QuestHands", QuestHandsUtility.CompressQuestHandsTransformToKeyStateAsString(m_questHandsMono.m_anchorReferences));
                Debug.Log("sent");
            }
        }
    }
}
