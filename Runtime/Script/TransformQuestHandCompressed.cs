﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class QuestHandsUtility 
{
   
    public static QuestHandsStateKey ReconstructStateFromCompressedString(string lines)
    {
        QuestHandsStateKey data = new QuestHandsStateKey();
      
        string[] spTokens = lines.Split('|');

        if (spTokens.Length != 52)
            return data;

        List<SpacePosition> positions = new List<SpacePosition>();
        for (int i = 0; i < spTokens.Length; i++)
        {
            positions.Add(SpacePositionFromStringToInstance(spTokens[i]));
        }
        
        data.m_ovrRoot = positions[0];
        data.m_centerEye = positions[1];
        data.m_left.m_wraistWorldPosition = positions[2];
        data.m_right.m_wraistWorldPosition = positions[3];
        data.m_left.m_bonesLocalPosition = positions.GetRange(4, 24).ToArray();
        data.m_right.m_bonesLocalPosition = positions.GetRange(28, 24).ToArray();
        
        return data;

    }

    public static QuestHandsStateKey tmp = new QuestHandsStateKey();
    public static string CompressQuestHandsTransformToKeyStateAsString(QuestHandsStateTransform target)
    {
        target.GetDataFromTransform(ref tmp);
        return CompressQuestHandsKeyStateAsString(tmp);
    }

    public static string CompressQuestHandsKeyStateAsString(QuestHandsStateKey target)
    {
        List<SpacePosition> spArray = new List<SpacePosition>();
        spArray.Add(target.m_ovrRoot);
        spArray.Add(target.m_centerEye);
        spArray.Add(target.m_left .m_wraistWorldPosition);
        spArray.Add(target.m_right.m_wraistWorldPosition);

      //  Debug.Log("D: " + m_currentHandsState.m_left.m_bonesLocalPosition.Length);
        foreach (SpacePosition sp in target.m_left.m_bonesLocalPosition)
        {
            spArray.Add(sp);
        }
        foreach (SpacePosition sp in target.m_right.m_bonesLocalPosition)
        {
            spArray.Add(sp);
        }
        
        return string.Join("|", spArray.Select(k => SpacePositionFromtInstanceToString(k)).ToList<string>());
    }

    /// <summary>
    /// px:pt:pz:rx:ry:rz:rw
    /// </summary>
    /// <param name="compressedText"></param>
    /// <returns></returns>
    private static SpacePosition SpacePositionFromStringToInstance(string compressedText)
    {
        string[] value = compressedText.Split(':');

        // Apparently float.Parse don't work the same in Unity 2020 between Win and Android. 
        // Win need 0,123 when android need 0.123. Weird need to find why.
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        for (int i = 0; i < value.Length; i++)
        {
            value[i] = value[i].Replace(".", ",");

        }
#endif

        SpacePosition sp = new SpacePosition();
        sp.m_position.x = float.Parse(value[0]);
        sp.m_position.y = float.Parse(value[1]);
        sp.m_position.z = float.Parse(value[2]);
        sp.m_rotation.x = float.Parse(value[3]);
        sp.m_rotation.y = float.Parse(value[4]);
        sp.m_rotation.z = float.Parse(value[5]);
        sp.m_rotation.w = float.Parse(value[6]);
        return sp;

    }
    public static string SpacePositionFromtInstanceToString(SpacePosition position) {

        return string.Format("{0:0.000}:{1:0.000}:{2:0.000}:{3:0.00}:{4:0.00}:{5:0.00}:{6:0.00}",
            position.m_position.x,
            position.m_position.y,
            position.m_position.z,
            position.m_rotation.x,
            position.m_rotation.y,
            position.m_rotation.z,
            position.m_rotation.w);
    }

    
}

#region Transform
[System.Serializable]
public class QuestHandsStateTransform
{
    public Transform m_ovrRoot;
    public Transform m_centerEye;
    public OculusHandAsTransform m_left;
    public OculusHandAsTransform m_right;
    private QuestHandsStateKey m_state= new QuestHandsStateKey();

    public QuestHandsStateKey GetDataFromTransform()
    {
        QuestHandsStateKey data = new QuestHandsStateKey();
        GetDataFromTransform(ref data);
        return data;
    }

    public void GetDataFromTransform(ref QuestHandsStateKey data)
    {
        data.m_ovrRoot.SetWithWorld(m_ovrRoot);
        data.m_centerEye.SetWithWorld(m_centerEye);
        data.m_left.SetWraistWith(m_left.m_wraistRoot);
        data.m_left.SetBonesWith(m_left.m_bonesTransform);
        data.m_right.SetWraistWith(m_right.m_wraistRoot);
        data.m_right.SetBonesWith(m_right.m_bonesTransform);
    }

    public enum HandsTransformDisplayType {
        FromUnityScene,
        FromLocalTransform,
        FromOculusCenterEye, 
        FromWraithRoot
    }
    public void SetFromDataToTransform(QuestHandsStateKey data, HandsTransformDisplayType displayType = HandsTransformDisplayType.FromUnityScene)
    {
        if (displayType == HandsTransformDisplayType.FromUnityScene)
        {
            m_ovrRoot.position = data.m_ovrRoot.m_position;
            m_ovrRoot.rotation = data.m_ovrRoot.m_rotation;


            m_centerEye.position = data.m_centerEye.m_position;
            m_centerEye.rotation = data.m_centerEye.m_rotation;

            m_left.m_wraistRoot.position = data.m_left.m_wraistWorldPosition.m_position;
            m_left.m_wraistRoot.rotation = data.m_left.m_wraistWorldPosition.m_rotation;
            SetInactiveIfNotDefined(m_left.m_wraistRoot, data.m_left.m_wraistWorldPosition);

            m_right.m_wraistRoot.position = data.m_right.m_wraistWorldPosition.m_position;
            m_right.m_wraistRoot.rotation = data.m_right.m_wraistWorldPosition.m_rotation;
            SetInactiveIfNotDefined(m_right.m_wraistRoot, data.m_right.m_wraistWorldPosition);

            for (int i = 0; i < 24; i++)
            {

                m_left.m_bonesTransform[i].localPosition = data.m_left.m_bonesLocalPosition[i].m_position;
                m_left.m_bonesTransform[i].localRotation = data.m_left.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_left.m_bonesTransform[i], data.m_left.m_bonesLocalPosition[i]);

            }

            for (int i = 0; i < 24; i++)
            {

                m_right.m_bonesTransform[i].localPosition = data.m_right.m_bonesLocalPosition[i].m_position;
                m_right.m_bonesTransform[i].localRotation = data.m_right.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_right.m_bonesTransform[i], data.m_right.m_bonesLocalPosition[i]);

            }
        }
        else if (displayType == HandsTransformDisplayType.FromLocalTransform)
        {
            m_ovrRoot.localPosition = data.m_ovrRoot.m_position;
            m_ovrRoot.localRotation = data.m_ovrRoot.m_rotation;


            m_centerEye.localPosition = data.m_centerEye.m_position;
            m_centerEye.localRotation = data.m_centerEye.m_rotation;

            m_left.m_wraistRoot.localPosition = data.m_left.m_wraistWorldPosition.m_position;
            m_left.m_wraistRoot.localRotation = data.m_left.m_wraistWorldPosition.m_rotation;
            SetInactiveIfNotDefined(m_left.m_wraistRoot, data.m_left.m_wraistWorldPosition);

            m_right.m_wraistRoot.localPosition = data.m_right.m_wraistWorldPosition.m_position;
            m_right.m_wraistRoot.localRotation = data.m_right.m_wraistWorldPosition.m_rotation;
            SetInactiveIfNotDefined(m_right.m_wraistRoot, data.m_right.m_wraistWorldPosition);

            for (int i = 0; i < 24; i++)
            {

                m_left.m_bonesTransform[i].localPosition = data.m_left.m_bonesLocalPosition[i].m_position;
                m_left.m_bonesTransform[i].localRotation = data.m_left.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_left.m_bonesTransform[i], data.m_left.m_bonesLocalPosition[i]);

            }

            for (int i = 0; i < 24; i++)
            {

                m_right.m_bonesTransform[i].localPosition = data.m_right.m_bonesLocalPosition[i].m_position;
                m_right.m_bonesTransform[i].localRotation = data.m_right.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_right.m_bonesTransform[i], data.m_right.m_bonesLocalPosition[i]);

            }
        }
        else if (displayType == HandsTransformDisplayType.FromOculusCenterEye)
        {
            m_ovrRoot.localPosition = data.m_ovrRoot.m_position;
            m_ovrRoot.localRotation = data.m_ovrRoot.m_rotation;


            m_centerEye.localPosition = data.m_centerEye.m_position;
            m_centerEye.localRotation = data.m_centerEye.m_rotation;
           

            m_left.m_wraistRoot.localPosition = data.m_left.m_wraistWorldPosition.m_position;
            m_left.m_wraistRoot.localRotation = data.m_left.m_wraistWorldPosition.m_rotation;
            SetInactiveIfNotDefined(m_left.m_wraistRoot, data.m_left.m_wraistWorldPosition);

            m_right.m_wraistRoot.localPosition = data.m_right.m_wraistWorldPosition.m_position;
            m_right.m_wraistRoot.localRotation = data.m_right.m_wraistWorldPosition.m_rotation;
            SetInactiveIfNotDefined(m_right.m_wraistRoot, data.m_right.m_wraistWorldPosition);

            


            for (int i = 0; i < 24; i++)
            {

                m_left.m_bonesTransform[i].localPosition = data.m_left.m_bonesLocalPosition[i].m_position;
                m_left.m_bonesTransform[i].localRotation = data.m_left.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_left.m_bonesTransform[i], data.m_left.m_bonesLocalPosition[i]);

            }

            for (int i = 0; i < 24; i++)
            {

                m_right.m_bonesTransform[i].localPosition = data.m_right.m_bonesLocalPosition[i].m_position;
                m_right.m_bonesTransform[i].localRotation = data.m_right.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_right.m_bonesTransform[i], data.m_right.m_bonesLocalPosition[i]);

            }
        }
        else if (displayType == HandsTransformDisplayType.FromWraithRoot)
        {
            m_ovrRoot.localPosition = Vector3.zero;
            m_ovrRoot.localRotation = Quaternion.identity;


            m_centerEye.localPosition = Vector3.zero;
            m_centerEye.localRotation = Quaternion.identity;

            m_left.m_wraistRoot.localPosition = Vector3.zero;
            m_left.m_wraistRoot.localRotation = Quaternion.identity;
            SetInactiveIfNotDefined(m_left.m_wraistRoot, data.m_left.m_wraistWorldPosition);

            m_right.m_wraistRoot.localPosition = Vector3.zero;
            m_right.m_wraistRoot.localRotation = Quaternion.identity;
            SetInactiveIfNotDefined(m_right.m_wraistRoot, data.m_right.m_wraistWorldPosition);

            for (int i = 0; i < 24; i++)
            {

                m_left.m_bonesTransform[i].localPosition = data.m_left.m_bonesLocalPosition[i].m_position;
                m_left.m_bonesTransform[i].localRotation = data.m_left.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_left.m_bonesTransform[i], data.m_left.m_bonesLocalPosition[i]);

            }

            for (int i = 0; i < 24; i++)
            {

                m_right.m_bonesTransform[i].localPosition = data.m_right.m_bonesLocalPosition[i].m_position;
                m_right.m_bonesTransform[i].localRotation = data.m_right.m_bonesLocalPosition[i].m_rotation;
                SetInactiveIfNotDefined(m_right.m_bonesTransform[i], data.m_right.m_bonesLocalPosition[i]);

            }
        }
        else {
            throw new System.NotImplementedException("I should code it but not now. Contact me if you need the implementation.");
        }


    }

    private void SetInactiveIfNotDefined(Transform transform, SpacePosition position)
    {
        transform.gameObject.SetActive(position.IsPositionDefined());
    }

    public Vector3 GetWorldPositionOf(HandType hand, FingerBones bone)
    {
        return GetTransformOf(hand, bone).position;
    }
    public Quaternion GetWorldRotationOf(HandType hand, FingerBones bone)
    {
        return GetTransformOf(hand, bone).rotation;
    }
    public Transform GetTransformOf(HandType hand, FingerBones bone)
    {
        return hand== HandType.Left? m_left.GetTransformOf(bone):m_right.GetTransformOf(bone);
    }

    public string GetStringCompressedVersion()
    {
        GetDataFromTransform(ref m_state);
        return m_state.GetStringCompressedVersion();
    }

    public void SetFromStringCompressedVersion(string compressedVersion, QuestHandsStateTransform.HandsTransformDisplayType relocationType)
    {
        QuestHandsStateKey state = QuestHandsUtility.ReconstructStateFromCompressedString(compressedVersion);
        SetFromDataToTransform(state, relocationType);
    }

 
}

[System.Serializable]
public class OculusHandAsTransform {
    public Transform m_wraistRoot;
    public Transform[] m_bonesTransform;

    public Transform GetTransformOf( FingerBones bone)
    {
        return m_bonesTransform[(int)bone];
    }
}
#endregion



#region Data

public class QuestHandsStateKey {

    public SpacePosition m_ovrRoot = new SpacePosition();
    public SpacePosition m_centerEye = new SpacePosition();
    public HandPositionKeyFrame m_left = new HandPositionKeyFrame();
    public HandPositionKeyFrame m_right = new HandPositionKeyFrame();


    public SpacePosition GetLocalPositionOfBone(HandType hand,  FingerBones bone) {
        return hand == HandType.Left ? m_left.GetLocalPositionOfBone(bone) : m_right.GetLocalPositionOfBone(bone);
    }
    public SpacePosition GetWorldPositionOf(OVRHandsKeyPoints keypoint) {
        switch (keypoint)
        {
            case OVRHandsKeyPoints.Root:return m_ovrRoot;
            case OVRHandsKeyPoints.CenterEyes: return m_centerEye;
            case OVRHandsKeyPoints.LeftHand: return m_left.GetWorldPosition();
            case OVRHandsKeyPoints.RightHand: return m_right.GetWorldPosition();
            default:return new SpacePosition();
        }
    }

    public bool IsHandTracked(HandType hand) {
        return hand == HandType.Left ? m_left.IsTracked() : m_right.IsTracked();

    }

    public string GetStringCompressedVersion()
    {
        return QuestHandsUtility.CompressQuestHandsKeyStateAsString(this);
    }
}

public class HandPositionKeyFrame {
    public SpacePosition m_wraistWorldPosition = new SpacePosition();
    public SpacePosition[] m_bonesLocalPosition = new SpacePosition[24];

    public HandPositionKeyFrame() {
        m_wraistWorldPosition= new SpacePosition();
        m_bonesLocalPosition = new SpacePosition[24];
        for (int i = 0; i < 24; i++)
        {
            m_bonesLocalPosition[i] = new SpacePosition();

        }
    }

    public void SetBonesWith(Transform[] bones)
    {
       // Debug.Log("s:" + bones.Length + " " + m_bonesLocalPosition.Length);
        int iCount = 0;
        while (iCount < bones.Length && iCount < 24) {
            if (m_bonesLocalPosition[iCount] == null)
                m_bonesLocalPosition[iCount] = new SpacePosition();
            m_bonesLocalPosition[iCount].SetWithLocal(bones[iCount]);
            iCount++;
        }
    }

    public void SetWraistWith(Transform wraist)
    {
        m_wraistWorldPosition.SetWithWorld(wraist);

    }

    public SpacePosition GetLocalPositionOfBone(FingerBones bone)
    {
        return m_bonesLocalPosition[(int)bone];
    }

    public SpacePosition GetWorldPosition()
    {
        return m_wraistWorldPosition;
    }
    public SpacePosition GetWorldPositionOf(FingerBones bone)
    {
        throw new System.NotImplementedException("I should code that but I don't have the head for rotation shit now. Contact me if you need it.");
    }

    internal bool IsTracked()
    {
        return m_wraistWorldPosition.IsPositionDefined();
    }
}
public class SpacePosition {

    public Vector3 m_position = new Vector3();
    public Quaternion m_rotation = new Quaternion();

    public void SetWithLocal(Transform value)
    {
        m_position = value.localPosition;
        m_rotation = value.localRotation;
    }
    public void SetWithWorld(Transform value)
    {
        m_position = value.position;
        m_rotation = value.rotation;
    }

    public bool IsPositionDefined()
    {
        return !IsToZeroPosition();
    }
    public bool IsToZeroPosition()
    {
        return m_position == Vector3.zero && m_rotation == Quaternion.identity;
    }
}
#endregion

public enum HandType { Left, Right}
public enum FingerBones:int {
    HandStart ,
    ForearmStub,
    HandThumb0,
    HandThumb1,
    HandThumb2,
    HandThumb3,
    HandIndex1,
    HandIndex2,
    HandIndex3,
    HandMiddle1,
    HandMiddle2,
    HandMiddle3,
    HandRing1,
    HandRing2,
    HandRing3,
    HandPinky0,
    HandPinky1,
    HandPinky2,
    HandPinky3,
    HandThumbTip,
    HandIndexTip,
    HandMiddleTip,
    HandRingTip,
    HandPinkyTip

}
public enum OVRHandsKeyPoints {
    Root, CenterEyes, LeftHand, RightHand
}