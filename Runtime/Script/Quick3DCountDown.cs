﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Quick3DCountDown : MonoBehaviour
{

    public Transform[] m_countDownObject;

    public int m_displayDigit;
    public float m_timer;
    public UnityEvent m_onEndCountDown;
    
    void Update()
    {
        if (m_timer <= 0) return;
        m_timer -= Time.deltaTime;
        if (m_timer <= 0)
        {
            m_displayDigit = 0;
            m_timer = 0;
            m_onEndCountDown.Invoke();
        }
        else {
            m_displayDigit = ((int)m_timer) + 1;
        }
        if (m_displayDigit < m_countDownObject.Length) {
            SetAllToFalse();
            m_countDownObject[m_displayDigit].gameObject.SetActive(true);
        }
        
    }

    private void SetAllToFalse()
    {
        for (int i = 0; i < m_countDownObject.Length; i++)
        {
            m_countDownObject[i].gameObject.SetActive(false);
        }
    }

    public void SetTimeTo(float time) { m_timer = time; }
    public void SetTimeTo(int time) { SetTimeTo((float)time); }
}
