﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandsRecordReplay : MonoBehaviour
{
    public TextAsset m_saveToReplay;

    [HideInInspector]
    public OculusHandsRecordAsString m_recordToReplay;
    [HideInInspector]
    public List<HandsRecordKey> m_keys;

    public QuestHandsStateTransformMono m_handDebug;
    public QuestHandsStateTransform.HandsTransformDisplayType relocationType;
    public float m_maxTime;

    public float m_currentTime;
    public float m_previousTime;
 
     void Start()
    {
        if (m_saveToReplay != null) {
            m_recordToReplay.m_recordKeys = m_saveToReplay.text;
            m_keys = OculusHandsRecordAsString.GetKeys(m_recordToReplay.m_recordKeys);
            m_keys = m_keys.OrderBy(k => k.m_time).ToList() ;
            m_maxTime = m_keys[m_keys.Count - 1].m_time;
        }
    }

    public void Update()
    {
        m_currentTime += Time.deltaTime;
        List<HandsRecordKey> found = m_keys.Where(k => k.m_time > m_previousTime && k.m_time <= m_currentTime).ToList();
        if (found.Count > 0)
        {
            m_handDebug.ReconstructFromStringVersion(found[found.Count-1].m_compressedVersion, relocationType);
        }

        if (m_currentTime > m_maxTime)
            m_currentTime = 0;
        m_previousTime = m_currentTime;
    }
}
