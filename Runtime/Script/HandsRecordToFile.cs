﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class HandsRecordToFile : MonoBehaviour
{
    public string m_editorRelativePath = "OculusHandsRecord/Record";
    public string m_androidRelativePath= "OculusHandsRecord/Record";

    public void StoreHandRecordAsFile(string fileName, OculusHandsRecordAsString record) {

        string directoryPath = "";
        string fileNameWithExtension = "/" + fileName + ".txt";
#if UNITY_EDITOR
        directoryPath = Application.dataPath + "/" + m_editorRelativePath ;
#endif
        if (Application.platform == RuntimePlatform.Android) {
            directoryPath = "/sdcard/" + m_androidRelativePath ;
        }
        Directory.CreateDirectory(directoryPath);
        File.WriteAllText(directoryPath + fileNameWithExtension, record.m_recordKeys);

    }
}
