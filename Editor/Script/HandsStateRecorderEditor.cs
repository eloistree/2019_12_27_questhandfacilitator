﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HandsStateRecorder))]
public class HandsStateRecorderEditor : Editor
{
    public string toSave;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        HandsStateRecorder myTarget = (HandsStateRecorder) target;
        GUILayout.BeginHorizontal();
        GUILayout.Label("Data:", GUILayout.Width(90));

        toSave = myTarget.m_handsInfo==null ? "": myTarget.m_handsInfo.GetStringCompressedVersion();
        EditorGUILayout.TextField(toSave);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Where to save:", GUILayout.Width(90));
        myTarget.m_whereToSaveLocal = EditorGUILayout.TextField(myTarget.m_whereToSaveLocal);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
            GUILayout.Label("Save:", GUILayout.Width(90));
        myTarget.m_nameOfTheRecord = EditorGUILayout.TextField(myTarget.m_nameOfTheRecord.Replace(" ", ""));
            GUILayout.Label(".txt");
            GUILayout.EndHorizontal();

        if (GUILayout.Button("Save State"))
        {
            string path = "";
            if (Directory.Exists(myTarget.m_whereToSaveLocal))
                path = myTarget.m_whereToSaveLocal;
            else path = Application.dataPath+"/"+myTarget.m_whereToSaveLocal ;

            Directory.CreateDirectory(path);
            File.WriteAllText(path + "/" + myTarget.m_nameOfTheRecord + ".txt", toSave);
            AssetDatabase.Refresh();

        }
            //myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);
            //EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
        }
}
