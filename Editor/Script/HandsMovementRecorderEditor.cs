﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(HandsMovementRecorder))]
public class HandsMovementRecorderEditor : Editor
{
    public string toSave;
    public override void OnInspectorGUI()
    {
        HandsMovementRecorder myTarget = (HandsMovementRecorder)target;

        if (myTarget.m_isRecording)
        {
            if (GUILayout.Button("Stop Recording"))
            {
                myTarget.StopRecording();
            }

        }
        else
        {
            if (GUILayout.Button("Start Recording"))
            {
                myTarget.StartRecording(myTarget.m_nameOfTheRecord);
            }

        }
        DrawDefaultInspector();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Size:", GUILayout.Width(90));
        EditorGUILayout.TextField(""+ (myTarget.m_currentRecord.m_recordKeys.Length/1000000f)+" Mo");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Where to save:", GUILayout.Width(90));
        myTarget.m_whereToSaveLocal = EditorGUILayout.TextField(myTarget.m_whereToSaveLocal);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Save:", GUILayout.Width(90));
        myTarget.m_nameOfTheRecord = EditorGUILayout.TextField(myTarget.m_nameOfTheRecord.Replace(" ", ""));
        GUILayout.Label(".txt");
        GUILayout.EndHorizontal();

     
        if (GUILayout.Button("Save Movement"))
        {
            string path = "";
            if (Directory.Exists(myTarget.m_whereToSaveLocal))
                path = myTarget.m_whereToSaveLocal;
            else path = Application.dataPath + "/" + myTarget.m_whereToSaveLocal;

            Directory.CreateDirectory(path);
            File.WriteAllText(path + "/" + myTarget.m_nameOfTheRecord + ".txt", myTarget.m_currentRecord.m_recordKeys);
            AssetDatabase.Refresh();

        }
        //myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);
        //EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
    }
}
